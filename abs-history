#!/bin/bash

# alfplayer, based on the script abslibre of dbscripts
# 2014-06-12
#
# "abs" command alternative fetching from Parabola's abslibre.git
# and from Arch's svntogit

ABSROOT="/var/abs-evc"
ABSLIBRE=/var/abs-evc/abslibre.git
ABSPARABOLA=/var/abs-evc/abs-parabola
# Remote
ARCHGITBASE=https://projects.archlinux.org/git/svntogit/
ABSLIBREGIT=https://projects.parabola.nu/abslibre.git
BLACKLISTURL='https://projects.parabola.nu/blacklist.git/plain/blacklist.txt'
BLFILE=/tmp/blacklist.txt

function sync_abs() {
# Sync Arch repos packages.git and community.git from internet
  for archrepo in packages community; do
    if [ -d ${ABSROOT}/arch-${archrepo}/.git ]; then
      pushd ${ABSROOT}/arch-${archrepo} >/dev/null 2>&1
      git pull
      popd  >/dev/null 2>&1
    else 
      git clone ${ARCHGITBASE}/${archrepo}.git ${ABSROOT}/arch-${archrepo}
    fi
  done

# Sync ABSLibre from internet
  if [ -d ${ABSLIBRE}/.git ]; then
    pushd ${ABSLIBRE} >/dev/null 2>&1
    git pull
    popd  >/dev/null 2>&1
  else 
    git clone ${ABSLIBREGIT} ${ABSLIBRE}
  fi
}

function get_blacklist() {
  printf ":: Updating blacklist...\t"
  wget -q -O - "${BLACKLISTURL}" | cut -d':' -f1 | sort -u | \
    sed "s#^#"${ABSROOT}"/arch-(packages|community)/#" > ${BLFILE} || {
      printf "[FAILED]\n"
      return 1
    }

# Prevent using an empty blacklist
  [ $(wc -l ${BLFILE} | cut -d " " -f1) -eq 0 ] && return 1

  printf "[OK]\n"
}

function sync_abs_libre() {

# Sync abslibre.git package repository directories
  if [ ! -d ${ABSPARABOLA} ]; then
    echo "Create directory ${ABSPARABOLA}"
    mkdir ${ABSPARABOLA}
    #ln -s ${ABSLIBRE}/{libre,libre-multilib,pcr,nonprism,java} ${ABSPARABOLA}
  fi

  ln -s ${ABSLIBRE}/{libre,libre-multilib,pcr,nonprism,java} ${ABSPARABOLA}

# Sync Arch packages symlinks
  mkdir "${ABSPARABOLA}"/{core,extra,community}
  for archrepo in packages community ; do
    git ls-tree --name-only @ -- arch-*/*/repos/*-* | \
    grep -vEf ${BLFILE} | while read -a path ; do
      file=${path##*/}
      read pkgname repo arch <<< $(echo "${path}" | sed 's#^\./arch-\(packages\|community\)/\([^/]*\)/repos/\([^/]*\)-\([^/]*\).*$#\2 \3 \4#')
        [[ $arch != i686 ]] && \
	  echo ln -s "${ABSROOT}/${archrepo}/${pkgname}/repos/${archrepo}-${arch}" "${ABSPARABOLA}/${repo}/${pkgname}"
    done
  done

  find -L "${ABSPARABOLA}" -type l -delete
}

# This part is very hacky and particular to the current setup :P
sync_pre_mips64el() {
  pushd /home/fauno/Repos/abslibre-pre-mips64el >/dev/null

  sudo -u fauno sh -c "

  rsync ${SYNCARGS} \
    --exclude=.git* \
    --exclude=community-staging \
    --exclude=community-testing \
    --exclude=gnome-unstable \
    --exclude=kde-unstable \
    --exclude=multilib \
    --exclude=multilib-testing \
    --exclude=multilib-staging \
    --exclude=staging \
    --exclude=testing \
    ${ABSLIBRE}/x86_64/ \
    /home/fauno/Repos/abslibre-pre-mips64el/ && \
    git add . && \
    git commit -m \"$(date)\" -a
    git push origin master
  "
}

# Create .abs.tar.gz tarballs
create_tarballs() {
  for repo in ${ABSLIBRE}/{i686,x86_64}/*; do
    baserepo=${repo##*/}
    arch=$(basename $(dirname $repo))

# Remove the old one
    mkdir -p /srv/http/web/media/abs/$baserepo/os/$arch/
    rm /srv/http/web/media/abs/$baserepo/os/$arch/$baserepo.abs.tar.gz
# Create a new one joining arch and any
# Remove the first part of the path (it could be $repo but any isn't hit)
    bsdtar -czvf /srv/http/web/media/abs/$baserepo/os/$arch/$baserepo.abs.tar.gz \
      -s ":${ABSLIBRE}/[a-z0-9_]\+/[a-z]\+::" \
        $repo/* ${ABSLIBRE}/any/${baserepo}/*

  done
}

sync_abs || exit 1
get_blacklist || exit 1
sync_abs_libre || exit 1
# This is being done at repo server now
#sync_pre_mips64el || exit 1
#create_tarballs || exit 1

#echo "Exclusion list used"
#cat ${BLFILE}
#less ${BLFILE}

exit 0
